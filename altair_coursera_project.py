# https://altair-viz.github.io/altair-viz-v4/gallery/multiple_interactions.html#multiple-interactions
import pandas as pd
import altair as alt

coursera_df = pd.read_csv("data/coursera_courses.csv")

# Preprocessing
ds_df = coursera_df[coursera_df["course_skills"].str.contains("Data Science")]

brush = alt.selection_interval()

bars = alt.Chart(ds_df).mark_bar().encode(y="course_organization",
                                                x="count(course_organization):N",
                                                color=alt.condition(brush, alt.value('steelblue'), alt.value('grey'))
                                               ).add_params(brush).properties(title=alt.Title(text='Orangization Histogram', subtitle="Click and drag down to select interval"))

ranked_text = alt.Chart(ds_df).mark_text(align='left').encode(
    y=alt.Y('row_number:O', axis=None)
).transform_window(
    row_number='row_number()'
).transform_filter(
    brush
)

c_rating = ranked_text.encode(text='course_rating:N').properties(
    title=alt.Title(text='Course Rating', align='left')
)

c_title = ranked_text.encode(text='course_title:N').properties(
    title=alt.Title(text='Course Title', align='left')
)

c_time = ranked_text.encode(text='course_time:N').properties(
    title=alt.Title(text='Course Time', align='left')
)


text = alt.hconcat(c_title,c_rating,c_time)# Combine data tables


index = alt.vconcat(
    bars,
    text
).configure_view(
    stroke=None
)

index.save('public/index.html')
